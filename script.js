
let x = 2;
const Getcube = x ** 3;
console.log(`The cube of 2 is ${Getcube}`);
const address = [258, "Washington Ave NW", "California",90011];
const [houseNumber,Street,State,ZipCode] = address;
console.log(` I live at ${houseNumber} ${Street}, ${State} ${ZipCode}`);
const animal = {
	name: "Lolong",
	description: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};

const {name,description,weight,measurement} = animal;
console.log(`${name} was a ${description}. He weighed at ${weight} with a measurement of ${measurement}`);
let Numbers = [1,2,3,4,5];
Numbers.forEach((number) => console.log(`${number}`));

const reduceNumber = Numbers.reduce((x,y) => x +y );
console.log(reduceNumber);

class Dog{
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
const AddDog = new Dog();
AddDog.name = "Frankie";
AddDog.age = 5;
AddDog.breed = "Miniature Dachshund";
console.log(AddDog);

